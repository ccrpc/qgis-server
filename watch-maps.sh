#!/bin/bash

MAPS=$(ls -l /maps | grep ".qg[sz]$")

while true; do
  sleep 10
  CURRENT=$(ls -l /maps | grep ".qg[sz]$")
  [[ "$MAPS" != "$CURRENT" ]] \
    && echo "Maps changed: stopping QGIS Server..." \
    && pkill qgis*
done
