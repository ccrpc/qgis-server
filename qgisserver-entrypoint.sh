#!/bin/bash

echo "Copying authentication database..."
mkdir -p "$QGIS_AUTH_DB_DIR_PATH"
cp -f /auth/* "$QGIS_AUTH_DB_DIR_PATH"
chmod -R 700 "$QGIS_AUTH_DB_DIR_PATH"
chown -R qgisserver:qgisserver "$QGIS_AUTH_DB_DIR_PATH"

echo "Setting server aliases..."
for path in /aliases/*
do
  server=$(basename $path)
  ip=$(getent hosts $(cat /aliases/$server) | awk '{ print $1 }')
  echo "$ip $server" >> /etc/hosts
done

echo "Watching maps for changes..."
/usr/bin/watch-maps.sh &

echo "Starting QGIS Server..."
exec "$@"
