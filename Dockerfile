FROM ubuntu:focal

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    gnupg \
    software-properties-common \
    wget \
  && wget -qO - https://qgis.org/downloads/qgis-2020.gpg.key | \
    gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import \
  && chmod a+r /etc/apt/trusted.gpg.d/qgis-archive.gpg \
  && add-apt-repository "deb https://qgis.org/ubuntu `lsb_release -c -s` main" \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    spawn-fcgi \
    qgis-server \
  && apt-get remove -y \
    gnupg \
    software-properties-common \
    wget \
  && apt-get autoremove -y \
  && rm -rf /var/lib/apt/lists/*

RUN groupadd -g 1500 qgisserver && \
  useradd -m -s /bin/bash -u 1500 -g 1500 qgisserver

COPY qgisserver-entrypoint.sh watch-maps.sh /usr/bin/

EXPOSE 9000
VOLUME /aliases
VOLUME /auth
VOLUME /maps
ENV QGIS_PROJECT_FILE="" \
  QGIS_SERVER_LOG_LEVEL=2 \
  QGIS_SERVER_LOG_STDERR=1 \
  QGIS_AUTH_DB_DIR_PATH="/home/qgisserver/auth/" \
  QGIS_AUTH_PASSWORD_FILE="/home/qgisserver/auth/qgis-auth-password"

ENTRYPOINT ["/usr/bin/qgisserver-entrypoint.sh"]

CMD ["su", "qgisserver", "-c", \
  "spawn-fcgi -n -p 9000 -- /usr/lib/cgi-bin/qgis_mapserv.fcgi"]
